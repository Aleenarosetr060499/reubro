package com.example.mylittilegreenthumb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<Module> listModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listModule = new ArrayList<>();
        listModule.add(new Module("gardening", "discription", R.drawable.gardening));
        listModule.add(new Module("crops", "discription", R.drawable.crops));
        listModule.add(new Module("products", "discription", R.drawable.shovel));
        listModule.add(new Module("Market place", "discription", R.drawable.market));
        listModule.add(new Module("gallery", "discription", R.drawable.gallery));
        listModule.add(new Module("Articles", "discription", R.drawable.articles));


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, listModule);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(myAdapter);
    }
}