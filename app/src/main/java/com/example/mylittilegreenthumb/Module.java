package com.example.mylittilegreenthumb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class Module extends AppCompatActivity {

    private String Title;
    private String Description;
    private int Image;

    public Module() {
    }

    public Module(String title, String description, int image) {
        Title = title;
        Description = description;
        Image = image;
    }

    public String getDescription() {
        return Description;
    }

    public int getImage() {
        return Image;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setImage(int image) {
        Image = image;
    }
}